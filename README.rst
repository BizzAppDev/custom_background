Project Has been moved to https://github.com/BizzAppDev-Systems/custom_background/
Installation
============

For testing it is also necessary to have the ``pypdf2`` app installed:

Ubuntu ::

    sudo pip3 install pypdf2
